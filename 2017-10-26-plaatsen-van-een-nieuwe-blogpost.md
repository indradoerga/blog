---
titel: Plaaten van een nieuwe blogpost
subtitel: via web of SmartGit of Atom
date: 2017-10-26
tags: ["blog", "blogging", "nieuw", "pagina", "post"]
---
Welkom
===================


Hey! Ik heb mijn blog wekenlijks en soms dagenlijk bijgehouden. Ik heb erin verwerkt wat mijn verbeterpunten zijn, wat ik anders ga doen of iets blijf doen. ik vertel ook over hoe alles is gegaan qua samenwerking, kennis en vaardigheden.

----------

Blogs
-------------

Hier staan de blogs die ik per week en sommige per dag heb bijgehouden.

> **05-09-2017 / 08-09-2017**
Vandaag heb ik een interview gehouden met 2 CMGT’ers. In totaal hebben 4 mensen van het team CMGT’ers geinterviewd. We hebben vragen gesteld over Rotterdam en over hun persoonlijke behoeften. Wat ik goed vond gaan was dat ik veel ging doorvragen dus vragen stellen en dan vragen van “Maar waarom dan”. Zo heb ik veel informatie kunnen halen uit de interviewee omdat ik nu te weten ben gekomen waarom ze een bepaald ding doen en niet alleen dat ze iets gewoon doen. Zelf had ik me wel beter willen voorbereiden, ik was oke voorbereid maar het kon nog beter. Ik vindt het goed van mezelf dat ik de nummer van de interviewee’s heb gevraagd. Dat is goed voor mijn observatie later en goed omdat ik dat normaal niet durf maar nu wel. Wij hebben alle informatie gekregen die we van de doelgroep nodig hadden. En hebben hier ons spel op gebaseerd. We hebben iederen concepten bedacht het aan elkaar verteld en elkaar feedback gegeven op de ideeen en het beste idee uitgekozen het idee alleen was niet ons uiteindelijk concept omdat we er nog toevoegingen aan hebben gebracht, die toevoegingen zijn de andere leden hun ideeen.



---
> **11-9-2017 / 15-09-2017**
Vandaag heb ik een hoorcollege gehad, mijn team en ik hebben vandaag de eerste stappen van de prototypen gedaan en hebben een plattegrond gehmaakt van de magic-circle van het spel. We hebben verschillende monumenten opgezocht op internet. We hebben afgesproken om de morgen een telefoon te maken voor de prototypen zodat het duidelijker is en mooier. We hebben alle prototypen getekend. Ook hebben de meeste deliverables vandaag af. Wat goed ging was de taakverdeling, iedereen was lekker aan het werk en had iets te doen. Wat ik minder goed vond gaan was dat we soms wel afgeleid raken. Dat is iets waar we allemaal aan moeten werken de een meer dan de ander. woensdag heb ik een studiodag gehad. De groep heeft de paperprototypen afgemaakt en iedereen heeft er lekker aan gewerkt. Alle deliverables zijn vandaag af en we geven elkaar feedback. Wat ik goed vond gaan was dat mijn team ook zonder mijn leiding aan het werk ging. Dat moet vanzelf al maar vandaag waren ze heel erg gedreven. Vandaag was alles goed gegaan geen enkel negatief iets. 
Op woendag heb ik ook mijn eerste workshop gehad. Het was workshop moodborad maken. Hier heb ik geleerd hoe ik moodboards maak. Ik heb wel eens eerder moodboards gemaakt. Bij deze les heb ik geleerd hoe ik met kleuren moet werken en niet alleen digitaal kan werken maar ook met tijdschriften en van alles. Dus dankzij deze les ben ik veel wijzer geworden over moodboards maken. Ik dacht dat ik het wist maar nu weet ik het nog beter.
![](moodboard.jpg)https://gitlab.com/indradoerga/blog/commit/38628e6cb27502b89cb6dc07f4ea99f370cad284#70a4eb4f2fd0dc11a71ee19a394da76e75ec515c

---
> **18-9-2017 / 22-9-2017**
Deze week hebben wij gewerkt aan de iteratie. De communicatie gaat steeds wat slechter. Mensen nemen bijvoorbeeld opeens pauzes terwijl er gewerkt moet worden. Hier moet ik  zelf iets aan doen denk ik omdat niemand er iets over zegt. Deze week was een korte week. Er is geen goede communicatie in het team en alles word gemaakt wanneer er zin is om het te maken. Ik wil de volgende keer beter communicatie binnen het team. Ik moet meer de leiding nemen anders wordt er niks afgemaakt.

---
> **25-9-2017**
Vandaag heb ik de hele dag  in de studio gezeten met mijn groepje.
Wat ik goed vond gaan is dat ik zelf de spelanalyse, SWOT en concepten vandaag af heb gemaakt. Wat ik minder goed vond gaan wat dat we vandaag als groepje weinig hebben gedaan. Ik denk dat dat vooral komt omdat niet iedereen een laptop heeft. 
Wat ik de volgende keer beter ga doen is mijn groep proberen te sturen om toch werk te verrichten. Werk wat geen laptop nodig heeft. Ook wil ik nog meer gedaan hebben op een dag en een betere schema maken zodat alles optijd klaar is voor mijzelf en de groep. Ook wil ik een lijst maken van alles wat we moeten inleveren zodat de lijst ook duidelijk is voor het team.

---
>**26-9-2017**
Vandaag heb ik een hoorcollege gehad over onderzoeken. Ik heb geleerd wat het verschil is tussen opzoeken, uitzoeken en onderzoeken.Ook heb ik geleerd over de verschillende denk niveaus. Ik heb geleerd dat de problem en solution samen loopt voor designers.
Wat ik beter wil doen de volgende keer is beter slapen voor e hoorcollege, omdat ik zo moe was had ik wat minder goed kunnen opletten dan ik meestal zou doen. Ik ga proberen op een betere manier aantekeningen te maken ik wil het visueler maken en dus minder woorden. Wat ik het zelfde wil doen is de hoeveelheid aan aantekeningen. Heel vaak schreef ik teveel dingen op ook al staat het al online, nu doe ik dat beter. Omdat ik nu minder schrijf kan ik meer opletten op wat de docent zegt.

---
>**27-9-2017**
Vandaag hebben wij gewerkt aan ons concept voor iteratie 2. Hier hebben wij een concept uit gekregen en hebben wij ewerkt aan de regels en naam van het spel.. Het bedenken van het 2de concept ging eerst eel moeilijk omdat het steeds verwarring was en we begonnen weer van nul. Al snel hebben we er toch een idee uit kunnen krijgen namelijk, pentaolympcs. Wat ik goed vond gaan was dat ik een teamlid heb kunnen overtuigen om wat beter mee te doen met ideeën bedenken. Dit is goed verlopen. Vandaag hebben wij feedback gekregen op ons nieuwe concept. Dit zal ik zo snel mogelijk gebruiken om het concept beter te maken. Het team begint al wat meer te werken maar nog niet genoeg naar mijn voorkeur maar we krijgen wel dingen af, daar ben ik blij mee.

---
>**28-9-2017**
Vandaag heb ik tijdens de werkcollege geleerd dat je een situatie op schillende manier kan onderverdelen en verschillend kunt zien. je hebt verschillende manier van iets voor iemand neerleggen en dat bepaal je helemaal zelf. Wat ik goed vond gaan was dat ik zelf en een andere persoon heel goed bezig waren en intensief terwijl de rest weinig tot bijna niks deed. Ik vond het goed dat ik heb kunnen inzien wat de bedoeling was van de les zonder uitleg. De volgende keer wil ik ook zo scherp zijn als vandaag en net zo intensief bezig zijn. Ik laat mensen te snel over mij heenlopen en daar moet ik echt iets aan doen, omdat ik meestal mensen bij de les houdt. 

---
>**02-10-2017 /06-20-2017**
Vandaag was ik erg productief, i heb samen met Pu-yi 4 kahoots gemaakt en het getest op mensen. Ook hebben we de Fearpong gestest. We hebben het gefilmt en de reacties van de proefpersonen bekeken. Verder hebben we vandaag gewerkt aan de paper prototypen. Wat ik vandaag minder goed vond gaan was dat ik niet precies wist waar we waren tijdens alle diliverables, omdat alles heel erg last minute en doorelkaar werd gemaakt.

---
>**09-10-2017 /13-10-2017**
Deze week zijn mijn team en ik begonnen aan iteratie 3. Alle feedback die er is ontvangen is verwerkt in de nieuwe iteratie. Als eerst is er gewerkt aan het uitwerken van de regels, de prototypes van het spel, ontwerpproceskaarten en de bijgewerkte spelanalyse. De samenwerking gin veel beter dan normaal. Ik heb meer leiding genomen dan ik normaal doe. Toen ik heel even de leiding nam werd er ook daadwerkelijk werk afgemaakt. Deze week heb ik geleerd dat ik wel eens de leiding mag geven en niet als een stil muisje moet gaan zitten en doen wat de ander zegt. Deze week heb ik een workshop gehad over STARRT maken. Ik heb geleerd hoe je de STARRT maakt en hoe je beoordeeld gaat worden. Deze les was erg informatie en we hebben veel interacties gehad tijdens de workshop. We moesten praten met elkaar en een paar moeten voor de klas staan. Ook moesten wij een eigen STARRT maken.
![](starrt.jpg)https://gitlab.com/indradoerga/blog/commit/7d616ac0aa75596450f1e1f0925899f36dde751c?view=parallel#4b4281d8491520bdd001397581bbe8e5617eb5ac

---
>**23-10-2017 / 25-10-2017**
Deze week is er gewerkt aan de expotafel. De denkwijze van het team was ongeveer gelijk op 1 persoon na. Die persoon heeft altijd wel iets te zeggen maar komt nooit met argumenten of andere ideeën waardoor derest van het team helemaal verward wordt van die persoon. Die persoon heeft aan de project bar weinig inzet getoont en staat alleen tafelvoetbal te spelen. Voor derest ging het wel goed. De expo is goed gegaan. In het begin was ik wel bang dat ik niet uit mijn woorden zou kunnen komen net als altijd. Maar de laatste tijd durf ik gewoon te zeggen wat ik wil zeggen onder onzeker over te komen. Ik ben daar ontzettend blij mee omdat ik al een tijdje last had van dat probleem. Ik neem meer de leiding en geef meer mijn mening. Deze week ging de communicatie veel beter dan normaal 

---
>**Kwartaal terugblik**
In het begin van het kwartaal ging de samenwerking steeds slechter tot een punt. Dat punt was eind iteratie 2. we hadden iteratie 2 zo uitgetseld dat was niet normaal. De samenwerking gin ontzettend slecht. Na iteratie 2 ging alles een stuk beter. De communicatie gin stukken beter en alle leden luisteren en doen actief mee. 
In het begin van het kwartaal wist ik niet veel over moodboards maken. Nu weet ik veel beter hoe ik de moodboards moet maken en de mening erachter. Ook heb ik nu een idee van wat een STARRT inhoud.
In het begin van het kwartaal durfde ik niet mijn meningen naar boven te brengen en aan het eind van het kwartwaal ging alles veel beter. Ik heb nu het gevoel dat ik beter uit mijn woorden kom dan eerst en dat ik overtuigend mijn mening kan geven aan anderen. Dus mijn vaardigheden zijn verbeterd.
In het begin van het kwartaal kwam ik met wat ervaring als CMD-er omdat ik kwam van De Haagse Hogeschool. Ik heb dankzij deze opleiding kunnen leren hoe ik de basis van een onderzoek moet doen en hoe ik die onderzoek gebruik om een uiteindelijk concept te maken. Mijn kennis is vergroot dankzij. Ik weet nu veel meer dankzij de hoor/werk colleges. 

